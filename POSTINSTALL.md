This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme123<br/>
**Email**: admin@cloudron.local<br/>

Please change the admin password and email immediately.

#### Important note on updates

Moodle is a complex application with a complicated upgrade procedure. It supports
over 25 different [types of plugins](https://docs.moodle.org/dev/Plugin_types) each
located in a different location in the source code. While we have automated the upgrade,
do not use any more plugins than necessary to reduce update issues. On the same note,
do not edit the source code of core moodle since it will be overwritten on an update.

