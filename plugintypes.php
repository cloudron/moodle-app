<?php
define('CLI_SCRIPT', true);
require('/app/data/moodle/config.php');
 
$pluginman = core_plugin_manager::instance();
 
# https://docs.moodle.org/dev/Plugin_types#Obtaining_the_list_of_plugin_types_known_to_your_Moodle
foreach ($pluginman->get_plugin_types() as $type => $dir) {
    $dir = substr($dir, strlen($CFG->dirroot));
    printf("%s".PHP_EOL, $dir);
}

