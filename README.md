# Moodle Cloudron App

This repository contains the Cloudron app package source for Moodle, an open-source Learning Management System (LMS).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.moodle.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id moodle.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd moodle

cloudron build
cloudron install
```

## Debugging

Put this in `/app/data/moodle/config.php`:

```
@error_reporting(E_ALL | E_STRICT);   // NOT FOR PRODUCTION SERVERS!
@ini_set('display_errors', '1');         // NOT FOR PRODUCTION SERVERS!
$CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
$CFG->debugdisplay = 1;              // NOT FOR PRODUCTION SERVERS!
```

## Usage

Use `cloudron push` to copy files into `/app/data/public/` and `cloudron exec` to get a remote terminal.

See https://cloudron.io/references/cli.html for how to get the `cloudron` command line tool.

## Tests

* Put `HashKnownHosts no` in your `~/.ssh/config`
* cd test
* npm install
* USERNAME=<> PASSWORD=<> mocha --bail test.js

