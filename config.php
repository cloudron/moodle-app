<?php
// https://docs.moodle.org/39/en/Configuration_file

unset($CFG);  // Ignore this line
global $CFG;  // This is necessary here for PHPUnit execution
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';      // 'pgsql', 'mariadb', 'mysqli', 'sqlsrv' or 'oci'
$CFG->dblibrary = 'native';     // 'native' only at the moment
$CFG->dbhost    = getenv('CLOUDRON_POSTGRESQL_HOST');  // eg 'localhost' or 'db.isp.com' or IP
$CFG->dbname    = getenv('CLOUDRON_POSTGRESQL_DATABASE');     // database name, eg moodle
$CFG->dbuser    = getenv('CLOUDRON_POSTGRESQL_USERNAME');   // your database username
$CFG->dbpass    = getenv('CLOUDRON_POSTGRESQL_PASSWORD');   // your database password
$CFG->prefix    = 'mdl_';       // prefix to use for all table names
$CFG->dboptions = array(
    'dbpersist' => false,       // should persistent database connections be
                                //  used? set to 'false' for the most stable
                                //  setting, 'true' can improve performance
                                //  sometimes
    'dbsocket'  => false,       // should connection via UNIX socket be used?
                                //  if you set it to 'true' or custom path
                                //  here set dbhost to 'localhost',
                                //  (please note mysql is always using socket
                                //  if dbhost is 'localhost' - if you need
                                //  local port connection use '127.0.0.1')
    'dbport'    => (int) getenv('CLOUDRON_POSTGRESQL_PORT'),          // the TCP port number to use when connecting
                                //  to the server. keep empty string for the
                                //  default port
    'dbhandlesoptions' => false,// On PostgreSQL poolers like pgbouncer don't
                                // support advanced options on connection.
                                // If you set those in the database then
                                // the advanced settings will not be sent.
    'dbcollation' => 'utf8mb4_unicode_ci', // MySQL has partial and full UTF-8
                                // support. If you wish to use partial UTF-8
                                // (three bytes) then set this option to
                                // 'utf8_unicode_ci', otherwise this option
                                // can be removed for MySQL (by default it will
                                // use 'utf8mb4_unicode_ci'. This option should
                                // be removed for all other databases.
    // 'fetchbuffersize' => 100000, // On PostgreSQL, this option sets a limit
                                // on the number of rows that are fetched into
                                // memory when doing a large recordset query
                                // (e.g. search indexing). Default is 100000.
                                // Uncomment and set to a value to change it,
                                // or zero to turn off the limit. You need to
                                // set to zero if you are using pg_bouncer in
                                // 'transaction' mode (it is fine in 'session'
                                // mode).
// For all database config settings see https://docs.moodle.org/en/Database_settings
);

$CFG->wwwroot   = getenv('CLOUDRON_APP_ORIGIN');
$CFG->dataroot  = '/app/data/moodledata';
$CFG->directorypermissions = 02777;
$CFG->admin = 'admin';
$CFG->sslproxy = true;
$CFG->dirroot = '/app/data/moodle';

// save sessions in redis (https://docs.moodle.org/310/en/Session_handling#Database_session_driver)
$CFG->session_handler_class = '\core\session\redis';
$CFG->session_redis_host = getenv('CLOUDRON_REDIS_HOST');
$CFG->session_redis_port = (int) getenv('CLOUDRON_REDIS_PORT');  // Optional.
$CFG->session_redis_database = 0;  // Optional, default is db 0.
$CFG->session_redis_auth = getenv('CLOUDRON_REDIS_PASSWORD'); // Optional, default is don't set one.
$CFG->session_redis_prefix = ''; // Optional, default is don't set one.
$CFG->session_redis_acquire_lock_timeout = 120;
$CFG->session_redis_acquire_lock_retry = 100; // Optional, default is 100ms (from 3.9)
$CFG->session_redis_lock_expire = 7200;
$CFG->session_redis_serializer_use_igbinary = false; // Optional, default is PHP builtin serializer.

// ensure to pick correct upstream ip
$CFG->getremoteaddrconf = 1;

$CFG->pathtophp = '/usr/bin/php';

# /report/security/index.php?detail=core_preventexecpath
$CFG->preventexecpath = true;

require_once('/app/data/moodle/lib/setup.php'); // Do not edit

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
