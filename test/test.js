#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    timers = require('timers/promises'),
    { Builder, By, until, Key } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const username = process.env.USERNAME, password = process.env.PASSWORD;

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser;
    let app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function visible(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn + '/login/index.php');
        await visible(By.id('loginbtn'));
        await browser.sleep(2000);
        await browser.findElement(By.id('username')).sendKeys(Key.chord(Key.CONTROL, 'a')); // some garbage is pre-filled sometimes
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.id('loginbtn')).click();
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Dashboard")]')), TEST_TIMEOUT);
    }

    async function addCourse() {
        await browser.get('https://' + app.fqdn + '/course/edit.php?category=0');
        await browser.wait(until.elementLocated(By.xpath('//h2[text()="Add a new course"]')), TEST_TIMEOUT);
        await browser.findElement(By.id('id_fullname')).sendKeys('Mathematics');
        await browser.findElement(By.id('id_shortname')).sendKeys('math');
        await browser.findElement(By.id('id_saveanddisplay')).click();
    }

    async function checkCourse() {
        await browser.get('https://' + app.fqdn + '/course/management.php');
        await browser.wait(until.elementLocated(By.xpath('//a[text()="Mathematics"]')), TEST_TIMEOUT);
    }

    async function checkAvailableCourses() {
        await browser.get('https://' + app.fqdn + '/enrol/index.php?id=2');
        await browser.wait(until.elementLocated(By.xpath('//a[text()="Mathematics"]')), TEST_TIMEOUT);
    }

    async function logout() {
        await clearCache();
    }

    async function checkCron() {
        console.log('Sleeping a bit to make sure cron has run');
        for (let i = 0; i < 10; i++) {
            await timers.setTimeout(60000);
            await browser.get(`https://${app.fqdn}/report/status/index.php`);
            await visible(By.xpath('//h2[text()="System status"]'));
            try {
                await browser.wait(until.elementLocated(By.xpath('//td[contains(text(), "Cron is running frequently")]')), TEST_TIMEOUT);
                return; // succeess
            } catch (err) {
                console.log('Will check again in 30 seconds');
            }
        }

        throw new Error('Cron did not work out!');
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('install app (no sso)', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000); // wait a bit before logging in
    });
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'changeme123'));
    it('can check cron', checkCron);
    it('can logout', logout);
    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // sso
    it('install app (sso)', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000); // wait a bit before logging in
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin', 'changeme123'));
    it('can add course', addCourse);
    it('can check cron', checkCron);
    it('can check course', checkCourse);
    it('can logout', logout);
    it('can student login', login.bind(null, username, password));
    it('can check course availability', checkAvailableCourses);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, 'admin', 'changeme123'));
    it('can check course', checkCourse);
    it('can check cron', checkCron);
    it('can logout', logout);
    it('can student login', login.bind(null, username, password));
    it('can check course availability', checkAvailableCourses);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        await browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', login.bind(null, 'admin', 'changeme123'));
    it('can check course', checkCourse);
    it('can check cron', checkCron);
    it('can logout', logout);
    it('can student login', login.bind(null, username, password));
    it('can check course availability', checkAvailableCourses);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'changeme123'));
    it('can add course', addCourse);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', login.bind(null, 'admin', 'changeme123'));
    it('can check course', checkCourse);
    it('can check cron', checkCron);
    it('can logout', logout);
    it('can student login', login.bind(null, username, password));
    it('can check course availability', checkAvailableCourses);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
