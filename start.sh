#!/bin/bash

set -eu

mkdir -p /run/moodle/sessions /app/data/moodledata

readonly src_dir=/app/data/moodle
readonly backup_src_dir=/app/data/moodle-prev-do-not-touch

# create proper dirs instead of symlinks. moodle gets confused with symlinks
rm -rf /app/data/moodledata/{temp,cache,localcache}
mkdir -p /app/data/moodledata/{temp,cache,localcache}

# we moved sessions to redis
rm -rf /app/data/moodledata/sessions && mkdir -p /app/data/moodledata/sessions

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/.initialized ]]; then
    echo "==> Fresh installation, performing Moodle first time setup"

    echo "==> Installing new moodle"
    rsync -az /app/code/new/ /app/data/moodle

    # not sure why this cannot run as www-data user
    php ${src_dir}/admin/cli/install_database.php --lang=en --adminuser=admin --adminpass=changeme123 --adminemail=admin@cloudron.local \
        --fullname='My Moodle Site' --shortname='MySite' --agree-license

    # the default installations sets auth to 'email'
    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        php ${src_dir}/admin/cli/cfg.php --name=auth --set="ldap"
    fi

    touch /app/data/.initialized
    echo "==> Installation done."
elif ! grep -q $MOODLE_VERSION /app/data/moodle/version.php; then
    echo "Version has changed. Will upgrade"
    echo "==> Moving existing installation"
    rm -rf ${backup_src_dir} && mv ${src_dir} ${backup_src_dir}
    echo "==> Installing new moodle"
    rsync -az /app/code/new/ /app/data/moodle

    # https://docs.moodle.org/dev/Plugin_types
    echo "==> Copying over user plugins from old installation"
    for subdir in `php /app/pkg/plugintypes.php`; do
        [[ ! -d ${backup_src_dir}/${subdir} ]] && continue

        echo "==> Plugin subdir ${subdir}"
        for x in `find "${backup_src_dir}/${subdir}"/* -maxdepth 0 -type d -printf "%f\n"`; do
            [[ -d "${src_dir}/${subdir}/${x}" ]] && continue  # do not overwrite plugin from the new version
            if [[ -d "/app/code/old/${subdir}/${x}" ]]; then
                echo "===> Skipping ${x} since it is missing in newer version, probably removed upstream"
            else
                echo "===> Copying user plugin ${x}"
                cp -rf "${backup_src_dir}/${subdir}/${x}" ${src_dir}/${subdir}
            fi
        done
    done

    echo "==> Upgrading moodle"
    # https://docs.moodle.org/39/en/Administration_via_command_line#Upgrading
    php ${src_dir}/admin/cli/upgrade.php --non-interactive

     rm -rf "${backup_src_dir}"
fi

# SMTP Setup
if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    php ${src_dir}/admin/cli/cfg.php --name=smtphosts --set="${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}"
    php ${src_dir}/admin/cli/cfg.php --name=smtpuser --set="${CLOUDRON_MAIL_SMTP_USERNAME}"
    php ${src_dir}/admin/cli/cfg.php --name=smtppass --set="${CLOUDRON_MAIL_SMTP_PASSWORD}"
    php ${src_dir}/admin/cli/cfg.php --name=noreplyaddress --set="${CLOUDRON_MAIL_FROM}"
fi

# LDAP Setup
if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    # migration code for config.php to db setting
    current_auth=$(php ${src_dir}/admin/cli/cfg.php --name=auth)
    [[ -z "${current_auth}" ]] && php ${src_dir}/admin/cli/cfg.php --name=auth --set="ldap"

    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=host_url --set="${CLOUDRON_LDAP_URL}"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=contexts --set="${CLOUDRON_LDAP_USERS_BASE_DN}"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=bind_dn --set="${CLOUDRON_LDAP_BIND_DN}"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=bind_pw --set="${CLOUDRON_LDAP_BIND_PASSWORD}"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=user_attribute --set="username"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=suspend_attribute --set="username"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_map_firstname --set="givenName"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_lock_firstname --set="locked"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_map_lastname --set="sn"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_lock_lastname --set="locked"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_map_email --set="mail"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_lock_email --set="locked"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_map_idnumber --set="username"
    php ${src_dir}/admin/cli/cfg.php --component=auth_ldap --name=field_lock_idnumber --set="locked"
fi

echo "==> Fixing permissions"
chown -R www-data:www-data /run/moodle /app/data
chown root:root /app/data/moodle/config.php # /report/security/index.php?detail=core_configrw

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
