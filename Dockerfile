FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/new /app/code/old /app/pkg
WORKDIR /app/code/new

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/moodle.conf /etc/apache2/sites-enabled/moodle.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN a2enmod rewrite headers php8.1 && a2dismod perl

# configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_input_vars 5000 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/moodle/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

ARG OLD_MOODLE_VERSION=4.4.4
# renovate: datasource=github-tags depName=moodle/moodle versioning=semver extractVersion=^v(?<version>.+)$
ENV MOODLE_VERSION=4.5.2
RUN curl -L https://github.com/moodle/moodle/archive/v${OLD_MOODLE_VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code/old
RUN curl -L https://github.com/moodle/moodle/archive/v${MOODLE_VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code/new

COPY config.php /app/code/new/config.php
COPY plugintypes.php start.sh /app/pkg/
COPY htaccess /app/code/new/.htaccess

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

CMD [ "/app/pkg/start.sh" ]
